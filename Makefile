HUGO_BIN ?= hugo
IMG_TYPES := jpg png gif
IMGS := $(foreach IMG, $(IMG_TYPES), $(wildcard static/img/*.$(IMG)))

help: ## Prints help for targets with comments
		@grep -E '^[a-zA-Z._-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

submodulize: ## Initializes and updates submodules
	git submodule update --init --recursive
	git submodule foreach git reset --hard
	git submodule sync

all: ## Builds the site
	$(HUGO_BIN) -d build

watch: ## Starts the dev server
	$(HUGO_BIN) -w --bind 0.0.0.0 serve

deps: ## Installs dependencies
	brew bundle

image-optim: ## Optimizes images
	for i in $(IMGS); do open -a ImageOptim.app $${i}; done

