+++
title = "Events we actively support"
+++

Listed below are some events that the Code & Supply Scholarship Fund its targetting for its scholarship awards.

Your event should be here if it's taking place in Pittsburgh. We try to keep
our ears to the ground but sometimes we miss events. [Let us know about
yours](mailto:scholarship@codeandsupply.co?subject=Event%20suggestion%3A%20XXXX&body=Hello%2C%20CSSF%21%20I%20have%20an%20event%20suggestion%20for%20you%3A%0A%0AEvent%20name%3A%0ADates%3A%0AWhere%3A%0A%0A%3C%21--%20If%20you%27re%20an%20organizer%20of%20the%20event%2C%20complete%20the%20below%21%20--%3E%0A%0A-%20%5B%20%5D%20We%20can%20provide%20X%20tickets%20for%20CSSF%20to%20distribute.%0A-%20%5B%20%5D%20We%20will%20mention%20CSSF%20and%20link%20to%20it%20from%20our%20web%20site.)
or [submit it on GitLab](https://gitlab.com/codeandsupplyfund/codeandsupply.fund/blob/master/content/events.md).

<!-- email template:

Hello, CSSF! I have an event suggestion for you:

Event name:
Dates:
Where:

----------- If you're an organizer of the event, complete the below!

- [ ] We can provide X tickets for CSSF to distribute.
- [ ] We will mention CSSF and link to it from our web site.

-->

**You can still [apply for a scholarship](/scholarships) to an event that is not listed here!**
This list is meant to inspire applicants to apply. It is not an exhaustive list
of events qualifying for CSSF support.

## Future events

| Day             | Conference                    | Fees | Notes     |
|-----------------|-------------------------------|------|-----------|
| 2021 October 22 | Bsides PGH Securty Conference | $10  | In-person |

## Past events on our radar

| Dates                   | Conference                                                                         | Fees                                                                                                                                    | Notes                              |
|-------------------------|------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|------------------------------------|
| 2021 May 12-20          | [PyCon 2021](https://www.pycon.org)                                                | 2020 prices: $125 student, $400 individual, $700 corporate                                                                              |                                    |
| 2020 October 1-3        | [Women in Statistics and Data Science](https://ww2.amstat.org/meetings/wsds/2020/) | Unknown                                                                                                                                 | Registration opens May 28, 2020    |
| 2020 August 12-15       | [Heartifacts 2020](https://heartifacts.codeandsupply.co)                           | $50                                                                                                                                     | Online; 2 awards; official partner |
| 2020 April 15-23        | [PyCon 2020](https://us.pycon.org/2020/)                                           | [Registration](https://us.pycon.org/2020/registration/): $120 student, $400 individual, $700 corporate (less with early bird discounts) | 1 award                            |
| 2020 March 30 - April 1 | [AIGA Pittsburgh 2020](https://pittsburgh.aiga.org/event/2020-design-conference/)  | $295 - $1,495                                                                                                                           |                                    |
| 2020 March 20-21        | [Women in Data Science Pittsburgh @CMU](http://www.stat.cmu.edu/wids/)             | free for high school students, $25 for undergrad/grad students, $75-$100 for non-students                                               |                                    |
| 2020 March 8-11         | [Code4Lib](https://2020.code4lib.org)                                              | $375 until February 10, $450 after; preconference days $50-$100                                                                         |                                    |
| 2019 August 21-23       | [Abstractions II](https://www.abstractions.io/)                                    | $50 student tickets, $300 GA tickets                                                                                                    | 47 awards; official partner        |
| 2019 June 28            | [BSides Pittsburgh](https://www.bsidespgh.com)                                     |                                                                                                                                         | no applicants                      |
| 2019 March 28-30        | [Women in cybersecurity](https://www.wicys.net/)                                   | [By member type](https://goo.gl/1vZFGN)                                                                                                 | no applicants                      |
| 2018 April 20-21        | [Heartifacts](https://codeandsupply.co/heartifacts)                                | $100                                                                                                                                    | 6 awards; Official partner         |
| 2018 April 18-19        | [Railsconf 2018](https://railsconf.org/)                                           |                                                                                                                                         | 12 awards; official partner        |
| 2017 August 24-25       | [Uptime](https://uptime.events/)                                                   | $200                                                                                                                                    | 3 awards; official partner         |
| 2016 August             | [Abstractions](https://www.abstractions.io)                                        | $200-$500                                                                                                                               | 57 awards; pre-incorporation       |
