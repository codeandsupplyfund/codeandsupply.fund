+++
title = "Frequently Asked Questions"
+++

## How do I apply for a scholarship?

Scholarship applications are open for limited events. [Apply now](https://goo.gl/forms/IDOdU6KvVTATFH6R2)

## What information do I need to provide for the application?

The application consists of your 

* Name
* Email and phone number so that we can contact you about the status of your
  application
* The event that you want to attend
    * You may need to provide the dates, location, and web site for the event if
      it's not in the list on the application.
    * See the pre-vetted events in our [events listing](/events).
* If you are scheduled to speak at the event
* Your travel details
    * From where you'll be traveling
    * Your expected expenses and total scholarship request amount
    * If you must have the entire amount in order to attend
* A short personal statement answering the question: "How does your background
  influence your interaction with the tech community, and why are you excited
  about attending this event?"

## What do I need to do if I am awarded a scholarship?

You will receive a scholarship contract outlining what is required of you in
order to receive the scholarship payment. The things include attending the
event in its entirety, abiding by its rules and the C&S Code of Conduct, etc.

Most importantly, you must attend the event in order to receive your
scholarship payment. Without prior arrangements, all of our scholarships are
paid _after_ the event based on expense reports and receipts showing
expenditures.

US citizens, resident aliens (green card holders), and student visa holders may
need to pay taxes on their award. We will collect an IRS Form W-9 and file
a 1099 if your award total for the year exceeds the reporting minimum, currently
$600 USD.

Non-resident US aliens (that's legalese for basically everyone but US citizens,
US green card holders, or non-US citizens present in the US on student visas)
may be required to complete a US IRS W-8BEN form. Our lawyers have told us that
income tax withholding is not required for scholarships under $3,000 USD and 
stays shorter than 90 days.

CSSF cannot provide tax advice. Please contact a tax professional if you have
questions about a scholarship's impact on your income taxes.

## Have others received awards? What was their outcome?

Some of our recipients have provided [testimonials](/testimonials/) about their
experience applying for and receiving a scholarship, including the logistics. 

Previous recipients can provide their testimonial via instructions on that page,
too.

## How can I help provide for scholarship awards?

You can [donate now](https://donorbox.org/codeandsupply-scholarship-donations-2018). Your tax-deductible donation will help us build funds in order to make an award. 

You can [contact us](mailto:scholarship@codeandsupply.co) about in-kind donations of tickets, travel credits, cryptocurrency, and other valuable, travel-related things.

## Who is behind this?

Code & Supply Scholarship Fund was started by three members of the Pittsburgh tech community:

*   Justin X. Reese, founder of [Code & Supply Co.](https://www.codeandsupply.co) and [Builder Code Works](http://www.builderworks.co/)
*   Colin Dean, software engineer and veteran event organizer in the Pittsburgh area
*   Brian Wongchaowart, software engineer and philanthropist

Three people currently serve on the board of directors.

* Justin X. Reese
* Colin Dean
* Heather Herrington

## Who comprises the selection committee?

The selection committee is comprised of an even number of active members of the 
Code & Supply community, with the president of the board of directors voting 
only when a tiebreaker is necessary.

## How can I join the selection committee or the board of directors?

You can volunteer your assistance by [contacting us](mailto:scholarship@codeandsupply.co).

## How are my donations spent?

We <strike>are</strike> will be transparent! C&S Scholarship Fund is committed to financial transparency as a §501\(c\)3 charitable organization. It will regularly publish financial reports beyond IRS requirements in order to build the public trust in its mission.

## What is the relationship between Code & Supply Scholarship Fund and Code & Supply Company?

Code & Supply Scholarship Fund, Inc. (CSSF) is a non-profit organization that was spun
out of Code & Supply Company, LLC (C&S) after the success of the scholarship program at
C&S' [Abstractions 2016](http://abstractions.io) conference. The two are
operated separately financially but share some of the same administrators, 
IT infrastructure, logos, and promotional channels.

