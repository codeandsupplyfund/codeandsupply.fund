---
title: Testimonials from recipients and donors
command_to_use_to_format_testimonials: >-
    fold -s -w 80 TESTIMONIAL.TXT | sed 's/^.*$/> &/'
---

Scholarship Fund awards are not just about helping someone have an experience.
That experience leads to insight, opportunity, and self-improvement that can
yield new friends, new business contacts, new jobs, and even new careers. These
**outcomes** are what drives our donors to help others.

Are you a recipient and want to [share your story](#recipient-testimonials)?
Are you a donor and want to [share why you donate](#donor-testimonials)?
See how at the bottom of the page.

## Recipient Testimonials

### Kelauni Cook, 2016 recipient

> Just 2.5 years ago, I moved to Pittsburgh to enter the tech industry as a
> former substitute teacher. Abstractions was the very first conference I'd
> attended in the city. Code & Supply embraced me as a "coding newbie" and
> understood that there was no way I'd be able to afford my ticket having moved
> here with just enough savings to afford my rent. I was able meet some amazing
> leaders in tech at Abstractions I, many of whom are still some of my greatest
> friends and mentors. But the cherry on top was my ability to network my way
> into **my very first software development job with The Washington Post**, which
> truly changed my life. Abstractions was awesome and I'm excited to now be able
> to pay that scholarship forward to another me at Abstractions II.

### James˚, 2018 recipient

Unemployed and discouraged after searching for a new job for several months, 
James˚ attended a conference using a CSSF award, found new enlightenment and 
rediscovered a love of the community, and was able to parlay that into a job 
where he was promoted within months of restarting his career at an employer that
more closely fit his culture expectations.

### Anonymous, 2019 recipient

> I found out about Abstractions II a couple weeks before it happened. As a junior 
> developer on a junior developer salary and student loans, I definitely didn't 
> think I could go. I applied for the scholarship and was accepted and was SO 
> excited. I ended up volunteering during the event as well and went to as many
> talks as I could. I met with some of the speakers after their talks because I
> was so inspired and wanted their insight on my own career questions and
> sometimes wanted more info on their experiences.

### Olivia Liddell, 2018 recipient

> In the Spring of 2018, I was thrilled to find out that I had been accepted to
> speak at the Heartifacts conference, and also that I had received a travel
> scholarship through the Code & Supply Scholarship Fund! I was excited to attend
> because I had never heard of such an event that specifically focused on the
> intersection between software development and mental health.
>
> At Heartifacts, I gave a talk on how to overcome fear of failure. The topic is
> very close to my heart because I’ve struggled with fear of failure and imposter
> syndrome for most of my life, and I was even afraid to submit my application for
> Heartifacts out of the fear that I would be rejected. 
>
> At the same time, I was also in the process of applying for new jobs. I had been
> working in the field of educational technology, and I was interested in finding
> a new role that would allow me to focus more on technical training. My first
> conference talk wasn’t recorded, so I decided to use my phone to make sure that
> I could get a recording of myself speaking at Heartifacts. Once I was back home
> in Chicago, I took a 7-minute clip of my talk, [uploaded it to YouTube as an
> unlisted video](https://www.youtube.com/watch?v=wfdEFb0UGWE), and then put the
> link onto my resume as I continued to apply for jobs.
>
> About a month later, I was in the final round of interviews at Cloudbakers, the
> company where I now work. As I met with several members of the team, including
> our CEO and CTO, everyone told me how watching that video of me speaking at
> Heartifacts played such a significant role in their decision to call me in for
> the interview… and then ultimately hire me. That also opened the door for me to
> speak with them about all of the other ways that I was positively impacted by my
> experience at Heartifacts, such as the importance of how to recognize and avoid
> burnout, and how to communicate in ways that are based on empathy and
> understanding.
>
> I’m incredibly grateful for the opportunity that I was given through the 
> Code & Supply Scholarship Fund. Being able to speak at Heartifacts truly changed
> my life for the better, and it has put me in a position where I am able to train
> and educate others, to help improve their lives, too.

### Anonymous, 2018 recipient

> As a student of a remote web development bootcamp living in a small town, 
> Code & Supply Scholarship Fund gave me the rare opportunity to meet and interact
> with real web developers in real life, for which I am very grateful. Without
> this scholarship, I would not have been able to network, make the contacts I
> made, or have exposure to new topics and ideas presented in the talks. I left
> the conference feeling more confident in my ability to be a successful
> professional in the web development industry.

### Ana Paula Gonzaga, 2024 recipient

> Having the opportunity to speak at the biggest Python event in the world was
> extremely important to me both personally and professionally. I live in Brazil,
> and the costs of this trip are enormous. Expenses like airfare, hotel, meals,
> and local transportation add up to an exorbitant amount. With the support of
> the C&S Scholarship [Fund], I was able to live this dream without having to go into
> debt. Thank you so much to everyone, especially Colin, who has been in touch
> with me from the beginning.

### Shani, 2019 recipient

> As a new dev and career changer this was my first conference. It was great to
> be able to see how many different topics were represented. I was happy to know
> so many people in the tech community; it was kind of surprising but also a 
> relief.

### Ari, 2019 recipient

> The C&S Scholarship Fund gave me the opportunity to attend the Abstractions
> II conference, an event I still talk about often and can’t recommend enough.
> I am so grateful to the scholarship committee for considering my application!
> Without the scholarship I know I wouldn’t have been able to set aside the
> funds necessary for getting to Pittsburgh for the event. The scholarship
> allowed me to see the trip as a reality and from the start, I was so excited
> in knowing I could be part of the Abstractions II community. The scholarship
> allowed me to be able to overcome a financial barrier so that I could look to
> learning so much more about areas like web accessibility and the various
> ways tech professionals think outside of the box to improve services for
> their audiences.
> 
> Beyond my main goal of learning more about accessibility in tech, at
> Abstractions II I was able to learn about UX research, looking at Javascript
> from an object oriented perspective, and tips for leaning into your bad ideas
> to find better ones, just to name a few. I also was able to appreciate the
> attention to detail in organizing an event that takes its commitment to
> diversity and the needs of whole individuals seriously, particularly with
> spaces that allowed you to have moments to check out from the experience, as
> well as opportunities highlighted for attendees to socialize with others at
> the event. After Abstractions II, I arrived back home with a wealth of notes
> and links to talks, and a list I am still going through of topics to discover
> or deepen my knowledge of, all from just the three days in Pittsburgh.
>
> While at Abstractions II there was an announcement made concerning the impact
> of the scholarship fund in terms of number of people it touches - for
> whatever number please add 35 and counting - following the event, I presented
> a talk for members of my cohort, all from typically underrepresented groups
> in tech, on my experience at the conference. This  largely discussed moving
> past imposter syndrome as well as some of the key topic takeaways I had from
> Abstractions II. Since then, when sharing resources and hopes for the future
> with others, particularly with groups associated with women in tech, I
> recommend Abstractions II conference and I have been encouraging those who
> would really benefit from being a participant to apply to the scholarship
> fund. This is an organization that offers, through the scholarship,
> opportunities with immediate, visible positive outcomes. I am so glad to have
> had this opportunity and am grateful to have learned so much and made new
> friends from having had this chance to attend Abstractions II. 

### Craig, 2019 recipient

> Attending Abstractions II (2019) was a life-changing experience, and yeah, I
> know, that sounds real cliché. But, as a graduate student studying abroad with
> essentially no background in coding or development, I had been looking for
> opportunities to deepen my skills in UI and learn about the technical aspects
> of the UX field without having to spend an arm and BOTH legs. While the
> combined cost of going to Abstractions II wasn't so much that I couldn't afford
> it, receiving a C&S scholarship made it that much more feasible for someone
> like me. And, the conference was, oh, so much more than I had expected.
> Throughout the short span of three days, I was inundated and with conversations
> of all angles from all kinds of people. At the conclusion, I was not only
> excited to continue building on my background in human-computer interaction,
> but also imbued with a desire for creativity and greatly inspired to improve my
> own methods, perspectives, and concepts for what it means to be a coder in this
> digital 21st century.
>
> TL;DR | Attendees, Don't hesitate and apply for funding if it'll help you make
> the decision. In the future, you can be the one giving back.
> Supporters, THANK YOU for allowing me to participate in an unique learning
> experience that has helped shaped who I am.

## Donor Testimonials

### Jon Daniel, Lead Infrastructure Engineer & a recurring donor

> I donate to the Code & Supply scholarship fund because access to education and
> opportunity is a critical part of a fair and just society. The kinds of
> opportunities tech provides cannot be reserved only for those who went to the
> right schools or grew up in the right zip codes.
>
> Pittsburgh’s tech community helped me financially at a time when I needed it
> by letting me attend a conference when I was between jobs and short on funds.
> This conference introduced me to a lot of amazing people and lead me down a
> path that greatly benefited my career. 
>
> Generosity like that needs to be made available to as many people as possible.
> I refuse to gatekeep this path to others and want to live in a society where
> everyone has the opportunity to pursue a good life and meaningful career.

## Recipients, tell us your outcomes! Donors, tell us why you support us!

Are you a recipient of a Scholarship Fund award? Share your story with us one of
of these ways:

* Fill out our [testimonial survey](https://goo.gl/forms/SemdIt5CGYBih6Qm2 "Code and Supply Scholarship Fund Testimonial Survey")
* Submit your story on 
[this website's repository](https://gitlab.com/codeandsupplyfund/codeandsupply.fund/blob/master/content/testimonials.md).
* Send an email to [scholarship@codeandsupply.co](mailto:scholarship@codeandsupply.co)

----

_(˚some names changed for privacy reasons)_
